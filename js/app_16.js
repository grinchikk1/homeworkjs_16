"use strict";

function fibonacci(F0, F1, n) {
  if (n === 0) {
    return F0;
  }
  if (n === 1) {
    return F1;
  }
  let currentF = F1;
  let prevF = F0;
  let nextF;
  for (let i = 2; i <= Math.abs(n); i++) {
    nextF = prevF + currentF;
    prevF = currentF;
    currentF = nextF;
  }
  if (n < 0 && n % 2 === 0) {
    return -currentF;
  }
  return currentF;
}

const F0 = 1;
const F1 = 2;
const n = prompt("Введіть порядковий номер числа Фібоначчі:");

const result = fibonacci(F0, F1, n);

const num = document.createElement("h1");
num.style.cssText =
  "position: absolute; top: 200px; left: 200px; font-size: 100px";
document.body.append(num);
num.textContent = result;
